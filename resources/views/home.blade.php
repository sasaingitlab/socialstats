@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Player Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif


                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Name</th>
                                <th scope="col">Desc</th>
                                <th scope="col">Social Media followers</th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach ($players as $player)

                                <tr>
                                    <th scope="row">{{$player->id}}</th>
                                    <td>{{$player->name}}</td>
                                    <td>{{$player->description}}</td>
                                    <td>
                                        @if(!empty($player->getSocialAccounts()))
                                            @foreach ($player->getSocialAccounts() as $socialAccount)
                                                {{$socialAccount->getFollowerCountType()}} : {{$socialAccount->getFollowerCount() == -1 ? 'Error fetching info' : $socialAccount->getFollowerCount() }} <br/>
                                            @endforeach
                                        @else
                                            {{ __('No accounts') }}
                                        @endif
                                    </td>
                                </tr>

                            @endforeach

                            </tbody>
                        </table>
                        <a href="{{route('runscheduler')}}">Refresh</a>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
