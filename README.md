## About 

- Making an API call to twitter/instagram and obtaining followers count is implemented.
- Plugable strategy class allows to introduce new social media connection without much of code change
- Laravel scheduler and background worker used to control rate limits

## Suggested improvements

- user interfaces with relevant controllers 
- end to end test to make sure the connectivity
- load test to reach rate limits and capture possible errors (and handle appropriately)
- use queue monitoring tool

## How rate limit will be handled 

- rate limit is specified along with connecting app's  settings (lets assume 60 per hour / per app / per user)
- scheduler will only fill the jobs on each round without exceeding the capacity while maintaining rate limits
- processing time can speedup by introducing new app settings
- there will be separate queue for each app setting
- it is better to use a queue monitor
- you can dynamically change the rate limit on table

## What happened when the job fails

- relevant social media processor class should throw various exceptions depends on the issue (not implemented)
- based on the exception Job will determine whether to try it again or not. (for permanent errors no point of retrying )
- eligibility/priority can be determined based on fetched_at/allow_retry values

## Setup

- composer install
- migrate 
- seed
- setup scheduler (/runscheduler for testing)
- start background worker (each app setting got its own queue)
    - php artisan queue:work --queue=twitter_1 
    - php artisan queue:work --queue=insta_2 
- visit /home to view dashboard 
