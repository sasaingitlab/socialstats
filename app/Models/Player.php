<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Player extends Model
{
    /**
     * Set one to many relationship
     */
    public function socialAccounts()
    {
        return $this->hasMany('App\Models\SocialAccount');
    }

    /**
     * Get initialized social accounts
     *
     * @return mixed
     */
    public function getSocialAccounts()
    {
        $socialAccountPresent = false;
        foreach ($this->socialAccounts as $socialAccount) {
            $socialAccount->initialize();
            $socialAccountPresent = true;
        }

        return $socialAccountPresent ? $this->socialAccounts : false;
    }
}
