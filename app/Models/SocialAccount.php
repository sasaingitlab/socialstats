<?php

namespace App\Models;

use App\Helpers\SocialAccountProcessor\DefaultProcessor;
use App\Helpers\SocialAccountProcessor\InstaProsessor;
use App\Helpers\SocialAccountProcessor\SocialStrategy;
use App\Helpers\SocialAccountProcessor\TwitterProcessor;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class SocialAccount extends Model
{
    private $strategy;

    /**
     * Initialize socialAccount by setting the appropriate strategy
     */
    public function initialize(): void
    {
        if ($this->type == 'insta') {
            $this->setStrategy(new InstaProsessor());
        } elseif ($this->type == 'twitter') {
            $this->setStrategy(new TwitterProcessor());
        } else {
            $this->setStrategy(new DefaultProcessor());
        }
    }

    /**
     * @param SocialStrategy $strategy
     */
    public function setStrategy(SocialStrategy $strategy): void
    {
        $this->strategy = $strategy;
    }

    /**
     * @param $settings
     */
    public function updateData(string $settings): bool
    {
        //TODO use getter and setter would be a better way to do this.
        $settingsArr = json_decode($settings, true);
        $this->data = $this->strategy->fetchInfo($settingsArr, $this->handle);
        return true;
    }

    /**
     * @return int
     */
    public function getFollowerCount(): int
    {
        return $this->strategy->getFollowerCount($this->data);
    }

    /**
     * @return string
     */
    public function getFollowerCountType(): string
    {
        return $this->strategy->getFollowerCountType();
    }

    /**
     * @param string $errorMsg
     */
    public function setLastError(string $errorMsg): void
    {
        $this->last_error = $errorMsg;
    }

    /**
     * @param bool $status
     */
    public function setAllowRetry(bool $status): void
    {
        $this->allow_retry = $status;
    }

    public function setFetchedAtNow(){
        $this->fetched_at = Carbon::now();
    }

}
