<?php

namespace App\Jobs;

use App\Models\AppSettings;
use App\Models\SocialAccount;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Redis;

class ProcessSocialInfo implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $account;
    protected $appSettings;

    public $tries = 1; // Since rate limit is handle by redis and jobs are waiting to obtain locks.
    public $timeout = 60; // Timeout seconds

    /**
     * ProcessSocialInfo constructor.
     * @param SocialAccount $account
     * @param AppSettings $appSettings
     */
    public function __construct(SocialAccount $account, AppSettings $appSettings)
    {
        $this->account = $account;
        $this->appSettings = $appSettings;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->account->initialize();
        $res = $this->account->updateData($this->appSettings->settings);
        if($res){
            $this->account->setAllowRetry(1);
            $this->account->setFetchedAtNow();
        }
        $this->account->save();
    }

    public function failed(\Exception $exception)
    {
        $this->account->setLastError($exception->getMessage());
        $this->setAllowRetry($exception);
        $this->account->save();
    }

    private function setAllowRetry($exception){
        //TODO: we need to throw system specific exceptions from each social media strategy. Based on that we decide whether we allow to retry fetching.
    }
}
