<?php

namespace App\Http\Controllers;

use App\Helpers\SocialAccountHelper;
use App\Models\Player;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home',
            [
                'players' => Player::where('active', 1)->get()
            ]);
    }

    public function runScheduler()
    {
        SocialAccountHelper::dispatchJobs();
        return $this->index();
    }
}
