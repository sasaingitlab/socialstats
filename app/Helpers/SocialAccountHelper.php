<?php

namespace App\Helpers;

use App\Jobs\ProcessSocialInfo;
use App\Models\AppSettings;
use App\Models\SocialAccount;

class SocialAccountHelper
{
    /**
     * Create next batch of jobs to update the social accounts
     *
     */
    public static function dispatchJobs()
    {
        $appSettings = AppSettings::where('active', 1)->get();

        foreach ($appSettings as $appSetting) {
            $queueName = $appSetting->type . '_' . $appSetting->id;
            $size = \Queue::size($queueName);
            $limit = ($appSetting->rate_limit_hour - $size) > 0 ? ($appSetting->rate_limit_hour - $size) : 1;
            $socialAccountsToUpdate = SocialAccount::where([
                ['type', '=', $appSetting->type],
                ['handle', '<>', NULL],
                ['active', '=', 1]
            ])->orderBy('fetched_at', 'asc')->limit($limit)->get();

            foreach ($socialAccountsToUpdate as $account) {
                ProcessSocialInfo::dispatch($account, $appSetting)->onQueue($queueName);
            }
        }
    }
}
