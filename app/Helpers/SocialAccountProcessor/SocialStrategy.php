<?php

namespace App\Helpers\SocialAccountProcessor;

interface SocialStrategy
{
    /**
     * Extract follower count
     *
     * @param $jsonData
     * @return mixed
     */
    public function getFollowerCount($jsonData): int;

    /**
     * @return string
     */
    public function getFollowerCountType(): string;

    /**
     * @param $settings
     * @param $screenName
     * @return string
     */
    public function fetchInfo($settings, $screenName): string;
}
