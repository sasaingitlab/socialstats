<?php


namespace App\Helpers\SocialAccountProcessor;


class InstaProsessor implements SocialStrategy
{

    public const FOLLOWER_COUNT_NAME = 'Insta Followers';

    /**
     * @param $jsonData
     * @return int
     */
    public function getFollowerCount($jsonData): int
    {
        $data = json_decode($jsonData, true);
        $followerCount = -1;
        if (isset($data['edge_followed_by']['count']) && is_numeric($data['edge_followed_by']['count'])) {
            $followerCount = $data['edge_followed_by']['count'];
        }

        return $followerCount;
    }

    /**
     * @return string
     */
    public function getFollowerCountType(): string
    {
        return self::FOLLOWER_COUNT_NAME;
    }

    /**
     * @param $settings
     * @param $screenName
     * @return string
     */
    public function fetchInfo($settings, $screenName): string
    {
        if (empty($screenName)) {
            throw new \Exception('User screen name is empty');
        }

        $endpoint = "https://www.instagram.com/$screenName/?__a=1";
        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', $endpoint, [
            'timeout' => 60, // Response timeout sec
            'connect_timeout' => 60, // Connection timeout sec
        ]);

        $content = $response->getBody();
        $data = json_decode($content, true);
        $userData = '';
        if (isset($data['graphql']['user'])) {
            $userData = $data['graphql']['user'];
        }

        if (!$this->validateJsonData(json_encode($userData))) {
            throw new \Exception('Error fetching user info');
        }

        return json_encode($userData);
    }

    /**
     * @param $json
     * @return bool
     */
    private function validateJsonData($json): bool
    {
        if ($this->getFollowerCount($json) == -1) {
            return false;
        }

        return true;
    }
}
