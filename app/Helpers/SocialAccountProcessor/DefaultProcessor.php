<?php


namespace App\Helpers\SocialAccountProcessor;


class DefaultProcessor implements SocialStrategy
{

    /**
     * @param $jsonData
     * @return int
     */
    public function getFollowerCount($jsonData): int
    {
        return 0;
    }

    /**
     * @return string
     */
    public function getFollowerCountType(): string
    {
        return 'N/A';
    }

    /**
     * @param $settings
     * @param $screenName
     * @return string
     */
    public function fetchInfo($settings, $screenName): string
    {
        return json_encode([]);
    }
}
