<?php

namespace App\Helpers\SocialAccountProcessor;

class TwitterProcessor implements SocialStrategy
{

    public const FOLLOWER_COUNT_NAME = 'Twitter Followers';
    public const USER_TIMELINE_ENDPOINT = 'https://api.twitter.com/1.1/statuses/user_timeline.json';
    public const USER_TIMELINE_FIELD = '?screen_name=';

    /**
     * Get the follower count based on the given json
     *
     * @param $jsonData
     * @return mixed|void
     */
    public function getFollowerCount($jsonData): int
    {
        $data = json_decode($jsonData, true);
        $followerCount = -1;
        if (isset($data['followers_count']) && is_numeric($data['followers_count'])) {
            $followerCount = $data['followers_count'];
        }

        return $followerCount;
    }

    /**
     * @return string
     */
    public function getFollowerCountType(): string
    {
        return self::FOLLOWER_COUNT_NAME;
    }

    /**
     * Make a request to twitter and fetch information
     *
     * @param $settings
     * @param $screenName
     * @return string
     * @throws \Exception
     */
    public function fetchInfo($settings, $screenName): string
    {
        if (empty($settings)) {
            throw new \Exception('App settings are empty');
        }

        if (empty($screenName)) {
            throw new \Exception('User screen name is empty');
        }

        //TODO: singleton pattern should be used
        $twitter = new \TwitterAPIExchange($settings);
        $json = $twitter->setGetfield(self::USER_TIMELINE_FIELD . $screenName)
            ->buildOauth(self::USER_TIMELINE_ENDPOINT, 'GET')
            ->performRequest(true, [CURLOPT_TIMEOUT => 60]);

        //get only user data
        $data = json_decode($json, true);
        $userDataJson = isset($data[0]['user']) ? json_encode($data[0]['user']) : null;

        if (!$this->validateJsonData($userDataJson)) {
            throw new \Exception('Error fetching user info');
        }

        return $userDataJson;
    }

    /**
     * @param $json
     * @return bool
     */
    private function validateJsonData($json): bool
    {
        if ($this->getFollowerCount($json) == -1) {
            return false;
        }

        return true;
    }
}
