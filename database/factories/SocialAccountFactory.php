<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\SocialAccount;
use Faker\Generator as Faker;

$factory->define(SocialAccount::class, function (Faker $faker) {
    $socialMediaTypes = ['insta', 'twitter'];
    $instaNames = ['tomnooksatm',
        'dinklebergskeptic',
        'harleysjoker',
        'shamilafan',
        'hogwartssortinghat',
        'lovebydaylight',
        'honorarycrystalgem',
        'rivervixen',
        'bugheadlover',
        'southsideserpent',
        'geminitwin',
        'mysticfallstimberwolves',
        'harrystyleslover',
        'jobrofan',
        'selena.fandom',
        'teamedwardforever',
        'endoftheline',
        'waywardsisters',
        'caosfan',
        'nickbrinashipper'];

    $twitterNames = [
        'BBCJonSopel',
        'StuffThatWorks1',
        'patcummins30',
        'jbairstow21',
        'GaleyLad',
        'bbc5live',
    ];

    $type = $socialMediaTypes[array_rand($socialMediaTypes)];
    $handle = $type == 'insta' ? $instaNames[array_rand($instaNames)] : $twitterNames[array_rand($twitterNames)];

    return [
        'type' => $type,
        'player_id' => function () {
            return \App\Models\Player::inRandomOrder()->first()->id;
        },
        'handle' => $handle,
        'allow_retry' => 1,
        'fetched_at' => \Carbon\Carbon::now(),
        'active' => 1
    ];
});
