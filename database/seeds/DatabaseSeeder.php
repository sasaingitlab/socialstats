<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $this->call(AppSettingsSeeder::class);
        $this->call(PlayerSeeder::class);
        $this->call(SocialAccountSeeder::class);
        Model::reguard();
    }
}
