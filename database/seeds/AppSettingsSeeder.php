<?php

use Illuminate\Database\Seeder;

class AppSettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('app_settings')->insert([
            'type' => 'twitter',
            'settings' => '{
                                "oauth_access_token": "486491360-9N83AX4XFiFyhhL5AsREF1S2ts9HcHqZn74RqN6O",
                                "oauth_access_token_secret": "UjZJRoUe8gFSyiFYzKvmrMblUXZ9xR2ppha5E5EcqAcS4",
                                "consumer_key": "jV8kDp82WHknRnAZZgibg",
                                "consumer_secret": "y4pwCn88CYwohBPVFhcJk0zEYYIxFM9l9PoqDoHi8s"
                            }',
            'rate_limit_hour' => 60,
            'active' => 1
        ]);

        DB::table('app_settings')->insert([
            'type' => 'insta',
            'settings' => '',
            'rate_limit_hour' => 60,
            'active' => 1
        ]);
    }
}
