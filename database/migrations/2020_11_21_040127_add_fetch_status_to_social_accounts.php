<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFetchStatusToSocialAccounts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('social_accounts', function (Blueprint $table) {
            $table->boolean('allow_retry')
                ->after('data')
                ->default(1);

            $table->text('last_error')
                ->after('allow_retry')
                ->nullable();

            $table->dateTime('fetched_at')
                ->after('last_error')
                ->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('social_accounts', function (Blueprint $table) {
            $table->dropColumn('allow_retry');
            $table->dropColumn('last_error');
            $table->dropColumn('fetched_at');
        });
    }
}
